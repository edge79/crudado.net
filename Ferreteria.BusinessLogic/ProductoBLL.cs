using Ferreteria.Entities;
using Ferreteria.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Ferreteria.BusinessLogic
{
	public class ProductoBLL : Conexion
	{
		private static ProductoBLL _instance;
		public static ProductoBLL Instance
		{
			get
			{
				if (_instance == null) _instance = new ProductoBLL();
				return _instance;
			}
		}

		public List<Productos> SelectAll()
		{
			using (SqlConnection conn = new SqlConnection(cadena))
			{
				conn.Open();
				return ProductoDAL.Instance.SelectAll(conn);
			}
		}

		public Productos Select(int id)
		{
			Productos _entity = null;
			using (SqlConnection conn = new SqlConnection(cadena))
			{
				conn.Open();
				return ProductoDAL.Instance.Select(id, conn);
			}
		}

		public bool Create(Productos entity)
		{
			using (SqlConnection conn = new SqlConnection(cadena))
			{
				conn.Open();
				return ProductoDAL.Instance.Create(entity, conn);
			}
		}

		public bool Delete(int id)
		{
			using (SqlConnection conn = new SqlConnection(cadena))
			{
				conn.Open();
				return ProductoDAL.Instance.Delete(id, conn);
			}
		}

		public bool Update(Productos entity)
		{
			using (SqlConnection conn = new SqlConnection(cadena))
			{
				conn.Open();
				return ProductoDAL.Instance.Update(entity, conn);
			}
		}
	}
}
