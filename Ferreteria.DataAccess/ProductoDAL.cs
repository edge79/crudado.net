using Ferreteria.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Ferreteria.DataAccess
{
	public class ProductoDAL
	{
		private static ProductoDAL _instance;
		public static ProductoDAL Instance
		{
			get
			{
				if (_instance == null) _instance = new ProductoDAL();
				return _instance;
			}
		}

		public List<Productos> SelectAll(SqlConnection conn)
		{
			List<Productos> listado = new List<Productos>();
			using (SqlCommand cmd = new SqlCommand("sp_ProductosSelectAll", conn))
			{
				using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.SingleResult))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					if (dr != null)
					{
						while (dr.Read())
						{
							Productos entity = new Productos()
							{
								
							};
							listado.Add(entity);
						}
					}
				}
			}
			return listado;
		}

		public Productos Select(int id, SqlConnection conn)
		{
			Productos _entity = null;
			using (SqlCommand cmd = new SqlCommand("sp_ProductosSelect", conn))
			{
				cmd.CommandType = CommandType.StoredProcedure;

				using (SqlDataReader dr = cmd.ExecuteReader(CommandBehavior.SingleRow))
				{
					if (dr != null)
					{
						while (dr.Read())
						{
							_entity = new Productos()
							{
								
							};
						}
					}
				}
			}
			return _entity;
		}

		public bool Create(Productos entity, SqlConnection conn)
		{
			using (SqlCommand cmd = new SqlCommand("sp_ProductosCreate", conn))
			{
				cmd.CommandType = CommandType.StoredProcedure;
				try
				{

					return cmd.ExecuteNonQuery() > 0;
				}
				catch
				{
					 throw new Exception("Problemas al insertar");
				}
			}
		}

		public bool Delete(int id, SqlConnection conn)
		{
			using (SqlCommand cmd = new SqlCommand("sp_ProductosDelete", conn))
			{
				cmd.CommandType = CommandType.StoredProcedure;
				try
				{

					return cmd.ExecuteNonQuery() > 0;
				}
				catch
				{
					 throw new Exception("Problemas para eliminar");
				}
			}
		}

		public bool Update(Productos entity, SqlConnection conn)
		{
			using (SqlCommand cmd = new SqlCommand("sp_ProductosUpdate", conn))
			{
				cmd.CommandType = CommandType.StoredProcedure;
				try
				{

					return cmd.ExecuteNonQuery() > 0;
				}
				catch
				{
					 throw new Exception("Problemas para actualizar");
				}
			}
		}
	}
}
