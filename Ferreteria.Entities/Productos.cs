﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ferreteria.Entities
{
	public class Productos
	{
		public int idProducts { get; set; }
		public string NombreProducto { get; set; }
		public float Precio { get; set; }
		public string Descripcion { get; set; }

	}
}
