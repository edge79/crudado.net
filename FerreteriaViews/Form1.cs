﻿using Ferreteria.BusinessLogic;
using Ferreteria.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FerreteriaViews
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}
		private void LimpiarControles()
		{
			tbxID.Text = "";
			tbxName.Text = "";
			tbxPrice.Text = "";
			tbxDescription.Text = "";
		}
		private void mostrar()
		{
			dataGridView1.DataSource = ProductoBLL.Instance.SelectAll();
		}
		private void BtnAction_Click(object sender, EventArgs e)
		{
			if(tbxID.Text.Length == 0)
			{
				Productos entity = new Productos();
				entity.NombreProducto = tbxName.Text.Trim();
				entity.Precio = float.Parse(tbxPrice.Text.Trim());
				entity.Descripcion = tbxDescription.Text.Trim();
				if (!ProductoBLL.Instance.Create(entity))
				{
					MessageBox.Show("Error al registrar");
				}
				else
				{
					LimpiarControles();
					mostrar();
				}

			}
			else
			{
				Productos entity = new Productos();
				entity.idProducts = int.Parse(tbxID.Text.Trim());
				entity.NombreProducto = tbxName.Text.Trim();
				entity.Precio = float.Parse(tbxPrice.Text.Trim());
				entity.Descripcion = tbxDescription.Text.Trim();
				if (!ProductoBLL.Instance.Update(entity))
				{
					MessageBox.Show("Error al modificar");
				}
				else {
					LimpiarControles();
					mostrar();
				}
			}

		}

		private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			tbxID.Text = (string)dataGridView1[0, e.RowIndex].Value;
			tbxName.Text = (string)dataGridView1[1, e.RowIndex].Value;
			tbxPrice.Text = (string)dataGridView1[2, e.RowIndex].Value;
			tbxDescription.Text = (string)dataGridView1[3, e.RowIndex].Value;
		}

		private void DataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			if (!ProductoBLL.Instance.Delete((int)dataGridView1[0, e.RowIndex].Value))
			{
				MessageBox.Show("Error al eliminar");
			}
			else
			{
				LimpiarControles();
				mostrar();
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			LimpiarControles();
			mostrar();
		}
	}
}
